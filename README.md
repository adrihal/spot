# Spot

![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)

Alternative and customizable session manager originally made for Solus budgie, but it can work in other gnome-based desktops too. It's coded in gjs and depends on it. This screenshot is from i3 window manager:

![Spot running in Budgie desktop](./screenshot.png)

## Features

- minimal graphical frontend
- keyboard friendly
- custom commands
- custom icons
- custom tooltips
- enable or disable what you want
- dark theme option

## Dependencies

`gjs`

## Installation

Just copy program somewhere in your $PATH if you want to access it from command line, or run it directly from location of your choice.
As an example:

```bash
git clone https://gitlab.com/MonsieurCaillou/spot
cd spot
chmod +x spot
```

**Install**
```bash
sudo cp spot /usr/bin
```

**Or run locally**
```bash
./spot
```

## Configuration

The config file is created at first launch in this location, with default options:
`~/.config/spot.conf`

**Default config**

Options are explicit in config file. Here are the defaults values:

```ini
[ui]
darktheme=true
tooltips=true
size=medium
closeonblur=true

[logout]
enable=true
icon=mail-replied-symbolic
tooltip=Logout
command=gnome-session-quit --logout --no-prompt
key=q

[lock]
enable=true
icon=system-lock-screen-symbolic
tooltip=Lock
command=systemctl suspend
key=l

[reboot]
enable=true
icon=view-refresh-symbolic
tooltip=Reboot
command=systemctl reboot
key=r

[shutdown]
enable=true
icon=system-shutdown-symbolic
tooltip=Shutdown
command=systemctl poweroff
key=s
```

**Icons names**

You can change icons with any name following the [Gnome icon naming specification](https://developer.gnome.org/icon-naming-spec/). The easiest way is to use the `gtk3-icon-browser` application to copy their names. Search in your package manager to install it.

**Icons size**

You can set icons sizes to: `small`, `medium` or `large` in the `[ui]` section.

**Custom config file**

You can specify your own config file path with the `--config` parameter, the file will be created with default options if doesn't exists.

`spot --config <your-path>/<your-config>.conf`

## Usage

`spot` to run the program with default config  
`spot --version` to output app version  
`spot --config <your-config-file-path>` to run with custom config  
Press `escape` key to close the window.


## Changelog

**Version 1.2**

- Added .desktop file
- New screenshot
- Added release tarball

**Version 1.1**

- Hide spot from taskbar and docks
- Basic keyboard shortcuts implementation

**Version 1.0**

- Initial release

## Todo

- [x] Enable window decorations
- [x] Close window when loose focus
- [x] Implement customizable keyboard shortcuts (via config file) for each action
- [x] Hide program from taskbar/docks

